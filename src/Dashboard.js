import React, { Component } from "react";
import PropTypes from "prop-types";
import moment from "moment";

//import io from "socket.io-client";
import {
  AgentFromXPHttpService,
  CreateTicketHttpService,
  GetAllTicketsHttpService
} from "../src/services/HttpService";
import {
  SOCKET,
  SIO_AGENT_FORM_XP,
  SIO_AGENT_PLATFORM,
  SIO_TICKET_STATS,
  SIO_TICKET_CHART,
  SIO_GET_LATEST_TICKET
} from "../src/constants/Constants";
import { SharedDataContext, DataSocketContext } from  "../src/components/app/UseContext";

//const socket = io(SOCKET.BASE_URL);

class Dashboard extends Component {
  constructor() {
    super();
    this.state = {
      statees: true,
      containerWidth: 0,
      containerHeight: 0,
      agentFromXP: [],
      agentFromPlatform: [],
      latestticket: [],
      agentCount: 0,
      ticketStats: { complete: 0, pending: 0, total: 0, new: 0 },
      ticketStatsChart: { new: 0, pending: 0, resolved: 0 },
      date: {},
      dateGraph: {
        date_start: "",
        date_end: ""
      }
    };
  }

  static contextType = SharedDataContext;

  load() {
    const context = this.context;
    const receptor = {
      headers: {
        "Content-Type": "application/json",
        publickey: context.sharedDataContext.currentUser.publickey,
        apisid: context.sharedDataContext.currentUser.apisid,
        sessionid: context.sharedDataContext.currentUser.session_id
      }
    };
    this.initSocketGetAgentPlatform(
      context.sharedDataContext.currentUser.userid,
      receptor
    );
  }

  loadAll() {
    const context = this.context;
    const receptor = {
      headers: {
        "Content-Type": "application/json",
        publickey: context.sharedDataContext.currentUser.publickey,
        apisid: context.sharedDataContext.currentUser.apisid,
        sessionid: context.sharedDataContext.currentUser.session_id
      }
    };
    let day = new Date();
    let momenday = moment(day);
    let dateDefault = momenday.format("YYYY-MM-DD HH:mm:ss").slice(0, 10);
    const date = {
      date_complete_start: dateDefault,
      date_complete_end: dateDefault,
      date_pending_start: dateDefault,
      date_pending_end: dateDefault,
      date_new_start: dateDefault,
      date_new_end: dateDefault,
      date_total_start: dateDefault,
      date_total_end: dateDefault
    };
    const dateGraph = {
      date_start: dateDefault,
      date_end: dateDefault
    };
    this.setState(
      {
        date,
        dateGraph
      },
      () => {
        this.initSocketTicketStats(this.state.date, receptor);
        this.initSocketTicketChartStats(this.state.dateGraph, receptor);
      }
    );
    this.initSocketGetAgentPlatform(
      context.sharedDataContext.currentUser.userid,
      receptor
    );
    this.initSocketAgentFromXP(receptor);
    this.initSocketLatestTicket(receptor);
    // this.initSocketTicketStats(this.state.date, receptor);
    this.updateWindowDimensions();
    window.addEventListener("resize", this.updateWindowDimensions);
    document.body.style.overflow = "hidden";
  }

  componentDidMount() {
    //  console.log("compoent did mount")
    this.loadAll();
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateWindowDimensions);
    document.body.style.overflow = "auto";
  }

  updateWindowDimensions = () => {
    this.setState({
      containerWidth: window.innerWidth,
      containerHeight: window.innerHeight
    });
  };

  //TICKET CHART STAT
  initSocketTicketChartStats = (date, receptor) => {
    // console.log("initSocketTicketChartStats ** : ");
    const { socket } = this.props;
    socket.on(SIO_TICKET_CHART, response => {
      //  console.log("SIO_TICKET_CHART : ", response);
      // this.onSocketTicketChartStats(response);
      this.setState({ ticketStatsChart: response.data });
    });

    GetAllTicketsHttpService.ticketStatsChartPerDate(date, receptor).then(
      response => {
        // console.log("ticketStatsChartPerDate : ", response);

        if (response.status === 200 || response.status === 202) {
          //  console.log("test success : ", response);
        }
      }
    );
  };
  // onSocketTicketChartStats = response => {
  //   // console.log("onSocketTicketChartStats : ", response.data);

  //   if (response && (response.status === 200 || response.status === 202)) {
  //     this.setState({ ticketStatsChart: response.data });
  //   }
  // };

  //4 TICKET LATEST

  initSocketLatestTicket = receptor => {
    //  console.log("initSocketLatestTicket ** : ");
    const { socket } = this.props;
    socket.on(SIO_GET_LATEST_TICKET, response => {
      //   console.log("SIO_GET_LATEST_TICKET : ", response);
      // this.onSocketLatestTicket(response);
      this.setState({ latestticket: response.data });
    });

    GetAllTicketsHttpService.getLatestTicket(4, receptor).then(response => {
      //  console.log("getLatestTicket : ", response);

      if (response.status === 200 || response.status === 202) {
        // console.log("test success : ", response);
      }
    });
  };
  // onSocketLatestTicket = response => {
  //   //console.log("onSocketLatestTicket : ", response.data);

  //   if (response && (response.status === 200 || response.status === 202)) {

  //   }
  // };

  // TICKET STATS
  initSocketTicketStats = (date, receptor) => {
    // console.log("initSocketTicketStats ** : ");
    const { socket } = this.props;
    socket.on(SIO_TICKET_STATS, response => {
      // console.log("SIO_TICKET_STATS : ", response);
      let ticketStats = {};
      let resp = [];
      resp = Object.entries(response.data);
      resp.length === 0
        ? (ticketStats = {
            agents: 0,
            complete: 0,
            pending: 0,
            new: 0,
            total: 0
          })
        : (ticketStats = response.data);
      //    this.onSocketTicketStats(response);
      this.setState({ ticketStats: ticketStats });
    });

    GetAllTicketsHttpService.ticketStatsPerDate(date, receptor).then(
      response => {
        //  console.log("ticketStatsPerDate : ", response);

        if (response.status === 200 || response.status === 202) {
          // console.log("test success : ", response);
        }
      }
    );
  };
  // onSocketTicketStats = response => {
  //   // console.log("onSocketTicketStats : ", response.data);

  //   if (response && (response.status === 200 || response.status === 202)) {
  //     this.setState({ ticketStats: response.data });
  //   }
  // };

  //GET AGENT PLATFORM
  // onSocketGetAgentPlatform = response => {
  //   //console.log("onSocketGetAgentPlatformfff: ", response);
  //   if (response && (response.status === 200 || response.status === 202)) {
  //     // console.log("onSocketGetAgentPlatform : ", response.data);
  //   }
  // };

  initSocketGetAgentPlatform = (userid, receptor) => {
    // console.log("initSocketGetAgentPlatform ** : ");
    const { socket } = this.props;
    socket.on(SIO_AGENT_PLATFORM, response => {
      //  console.log("SIO_AGENT_PLATFORM : ", response);
      //this.onSocketGetAgentPlatform(response);
      this.setState({ agentFromPlatform: response.data });
      this.setState({ agentCount: response.data.length }, () => {
        localStorage.setItem(
          "agentCount",
          JSON.stringify(this.state.agentCount)
        );
      });
    });

    CreateTicketHttpService.getAgentPlatFrom(userid, receptor).then(
      response => {
        // console.log("getAgentPlatFrom : ", response);

        if (response.status === 200 || response.status === 202) {
          //   console.log("test success : ", response);
        }
      }
    );
  };

  //GET AGENT FROM XP
  initSocketAgentFromXP = receptor => {
    //   console.log("initSocketAgentFromXP : **** ");
    const { socket } = this.props;
    socket.on(SIO_AGENT_FORM_XP, response => {
      // this.onSocketGetAgentFromXP(response);
      this.setState({ agentFromXP: response.data });
    });

    AgentFromXPHttpService.getDatasAgentFromXP(receptor).then(response => {
      //   console.log("getDatasAgentFromXP : ", response);

      if (response.status === 200 || response.status === 202) {
        //  console.log("test success : ", response);
      } else {
        // console.log('test error : ', response);
      }
    });
  };

  // onSocketGetAgentFromXP = response => {
  //   //console.log("onSocketGetAgentFromXP : ", response.data);

  //   if (response && (response.status === 200 || response.status === 202)) {
  //     // this.setState({ agentFromXP: response.data });
  //   }
  // };

  ticketCompleteDateFilter = (timeStart, timeEnd) => {
    // console.log('timeStart',timeStart)

    const context = this.context;
    const receptor = {
      headers: {
        "Content-Type": "application/json",
        publickey: context.sharedDataContext.currentUser.publickey,
        apisid: context.sharedDataContext.currentUser.apisid,
        sessionid: context.sharedDataContext.currentUser.session_id
      }
    };
    let day = new Date();
    let momenday = moment(day);
    //let dateDefault = momenday.format("YYYY-MM-DD HH:mm:ss").slice(0, 10);

    const timeStartmoment = moment(timeStart)
      .format("YYYY-MM-DD HH:mm:ss")
      .slice(0, 10);
    const timeEndmoment = moment(timeEnd === null ? momenday : timeEnd)
      .format("YYYY-MM-DD HH:mm:ss")
      .slice(0, 10);
    //console.log(timeEndmoment)
    this.setState(
      {
        date: {
          ...this.state.date,
          date_complete_start: timeStartmoment,
          date_complete_end: timeEndmoment
        }
      },
      () => {
        this.initSocketTicketStats(this.state.date, receptor);
      }
    );
  };

  ticketPendingDateFilter = (timeStart, timeEnd) => {
    // console.log('timeStart',timeStart)
    // console.log('timeEnd',timeEnd)
    const context = this.context;
    const receptor = {
      headers: {
        "Content-Type": "application/json",
        publickey: context.sharedDataContext.currentUser.publickey,
        apisid: context.sharedDataContext.currentUser.apisid,
        sessionid: context.sharedDataContext.currentUser.session_id
      }
    };
    let day = new Date();
    let momenday = moment(day);
    // let dateDefault = momenday.format("YYYY-MM-DD HH:mm:ss").slice(0, 10);

    const timeStartmoment = moment(timeStart)
      .format("YYYY-MM-DD HH:mm:ss")
      .slice(0, 10);
    const timeEndmoment = moment(timeEnd === null ? momenday : timeEnd)
      .format("YYYY-MM-DD HH:mm:ss")
      .slice(0, 10);
    this.setState(
      {
        date: {
          ...this.state.date,
          date_pending_start: timeStartmoment,
          date_pending_end: timeEndmoment
        }
      },
      () => {
        this.initSocketTicketStats(this.state.date, receptor);
      }
    );
  };

  ticketTotalDateFilter = (timeStart, timeEnd) => {
    const context = this.context;
    const receptor = {
      headers: {
        "Content-Type": "application/json",
        publickey: context.sharedDataContext.currentUser.publickey,
        apisid: context.sharedDataContext.currentUser.apisid,
        sessionid: context.sharedDataContext.currentUser.session_id
      }
    };
    let day = new Date();
    let momenday = moment(day);
    // let dateDefault = momenday.format("YYYY-MM-DD HH:mm:ss").slice(0, 10);
    const timeStartmoment = moment(timeStart)
      .format("YYYY-MM-DD HH:mm:ss")
      .slice(0, 10);
    const timeEndmoment = moment(timeEnd === null ? momenday : timeEnd)
      .format("YYYY-MM-DD HH:mm:ss")
      .slice(0, 10);
    this.setState(
      {
        date: {
          ...this.state.date,
          date_total_start: timeStartmoment,
          date_total_end: timeEndmoment
        }
      },
      () => {
        this.initSocketTicketStats(this.state.date, receptor);
      }
    );
  };

  graphTicketDateFilter = (timeStart, timeEnd) => {
    //  console.log("timeEnd", timeEnd);
    const context = this.context;
    const receptor = {
      headers: {
        "Content-Type": "application/json",
        publickey: context.sharedDataContext.currentUser.publickey,
        apisid: context.sharedDataContext.currentUser.apisid,
        sessionid: context.sharedDataContext.currentUser.session_id
      }
    };
    // console.log("context.sharedDataContext",context.sharedDataContext)
    let day = new Date();
    let momenday = moment(day);
    let dateDefault = momenday.format("YYYY-MM-DD HH:mm:ss").slice(0, 10);
    const timeStartmoment = moment(timeStart)
      .format("YYYY-MM-DD HH:mm:ss")
      .slice(0, 10);
    const timeEndmoment = moment(timeEnd === null ? momenday : timeEnd)
      .format("YYYY-MM-DD HH:mm:ss")
      .slice(0, 10);

    this.setState(
      {
        dateGraph: {
          date_start: timeStartmoment,
          date_end: timeEndmoment
        }
      },
      () => {
        //   console.log(this.state.dateGraph);
        this.initSocketTicketChartStats(this.state.dateGraph, receptor);
      }
    );
  };

  render() {
    const { t, kind, defaultLang, changeLang, isLogged, i18n } = this.props;
    const {
      containerHeight,
      containerWidth,
      agentFromXP,
      agentCount,
      agentFromPlatform,
      ticketStats,
      latestticket,
      date,
      ticketStatsChart
    } = this.state;
    //console.log('render me',this.props)
    //   let days = new Date();
    //   let momenday= moment(days)
    //  // let dateDefault=momenday.format('MM/DD/YYYY HH:mm:ss').slice(0,11)
    //console.log("date", date);
    // console.log('ticketStats',ticketStats)
    return (
      <div className={`${kind === "dashboard" ? "columns" : ""}`}>
       
      </div>
    );
  }
}

Dashboard.propTypes = {
  i18n: PropTypes.shape({}).isRequired,
  t: PropTypes.func.isRequired,
  kind: PropTypes.string.isRequired,
  defaultLang: PropTypes.shape({}).isRequired,
  changeLang: PropTypes.func.isRequired,
  isLogged: PropTypes.bool.isRequired
};

export default Dashboard;
