import React from 'react';
import { Router } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import styled from 'styled-components';
import Loading from './Loading';
import Dashboard from './Dashboard';

const MainColumn = styled.div`
  max-width: 1150px;
  margin: 0 auto;
`;

const defaultHistory = createBrowserHistory();

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      dashboard: [],
      error: false
    };
  }


  render() {
    const {
      dashboard,
      loading,
      error,
    } = this.state;


    if (error) {
      return (
        <MainColumn>
          Sorry, 
        </MainColumn>
      );
    }

    return (
      <Router history={this.props.history || defaultHistory}>
        <MainColumn>
          <Dashboard
            dashboard={dashboard}
          />
        </MainColumn>
      </Router>
    );
  }
}

export default App;
